from django.shortcuts import render
from .models import Usuario
from .models import Meta

# Create your views here.

def listar(request):
	usuarios = Usuario.objects.all()
	return render(request, 'CrudUsuario/listar.html',
	{'usuarios' : usuarios})

def post_list(request):
    user = request.user
    if user.has_perm('CrudUsuario.administrador'):
        posts = Post.object.filter(
            published_date__lte=timezone.now()).order_by('published_date')
        return render(request, 'CrudUsuario/listar.html',{'posts': posts})
    else:
        return render(request, 'CrudUsuario/home.html')