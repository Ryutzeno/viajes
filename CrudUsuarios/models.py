from django.db import models
from django.utils.translation import ugettext as _
# Create your models here.

class Usuario(models.Model):
        nombre = models.CharField(max_length=50,
            help_text='p. ej. Juan')
        apellido = models.CharField(max_length=50,
            help_text='p. ej. Perez')
        rut = models.CharField(max_length=10,
            help_text='p. ej. 20311070-7')
		
        def __str__(self):
            return self.rut
        
        class Meta:
            permissions = (
                ('administrador',_('Es administrador')),
                ('usuario',_('Es usuario')), 
                )            
    