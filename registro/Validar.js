/**
Validar.js
Valida los datos de registro de usuario
*/

$(document).ready(function(){
	$("[name='usname']").blur(validarNombreUsuario)	

	$("#form1").submit(function(evento){
		if(!validar()){
			alert("El formulario contiene errores");
			// Si el formulario no valida, no envia los datos
			evento.preventDefault();
		}		
	})
});

function validar(){ 
	if(!validarNombreUsuario()){
		return false;
	}
	if(!validarCorreo()){
		return false;
	}
}


function validarNombreUsuario() {
	$("#mensaje_nombre_usuario").html("");
	//El nombre deberia estar al menos completo...
	var nombre = $("[name='usname']").val();
	nombre = $.trim(nombre); //sacamos los espacios vacios
	if(nombre.length == 0) {
		$("#mensaje_nombre_usuario").html(
		"El nombre no debe quedar en blanco");
		
	}
	return (nombre.length > 0);
}

function validarCorreo() {
	$("#mensaje_correo").html("");		
	// El correo debe estar al menos completo...
	var correo = $("[name='usemail']").val();
	correo = $.trim(correo); // sacamos los espacios vacios
	if(correo.length == 0)  {
		$("#mensaje_correo").html(
					"El correo no debe quedar en blanco");		
	}
	return (correo.length > 0);
}

